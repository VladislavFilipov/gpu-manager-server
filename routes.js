const mysql = require("mysql2");
const RemoteConnection = require('./remote-connection');

const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PSW
});

let cardsData = [];

connection.connect(function (err) {
    if (err) {
        return console.error("Ошибка: " + err.message);
    }
    else {
        console.log("Подключение к серверу MySQL успешно установлено");
        // let servers = [{
        //     id: 1,
        //     host: '91.238.230.147',
        //     port: 2236,
        //     username: 'researcher',
        //     password: 'NI!pa078'
        // }];

        // let query = `select * from servers;`;

        // console.log(query);
        connection.query(`select * from servers;`, (err, servers, fields) => {
            if (err) {
                console.log(err);
            } else {
                // console.log(results[0]);
                getCardsData(servers, init = true);
                setInterval(() => {
                    getCardsData(servers);
                }, 30000);
            }
        });


        // getCardsData(servers);
    }
});

function getCardsData(servers, init = false) {
    servers.forEach(server => {
        new RemoteConnection('ssh2', server, (err, conn) => {
            if (err) throw err;

            conn.on('data', (data) => {
                cardDataAdded(`${data}`, server.id, init);
                // console.log(data);
            });

            conn.on('error', (err) => {
                throw new Error(`Error: ${err}`);
            });

            conn.on('close', (data) => {
                console.log(`Close: ${data}`);
            });

            conn.write(`nvidia-smi --query-gpu=name,uuid,fan.speed,temperature.gpu,pstate,power.draw,power.limit,memory.used,memory.total,utilization.gpu --format=csv\n`);
            // emitter.emit('RemoteConnectionCreated', conn);
        });
    });
}

function cardDataAdded(data, serverId, init) {
    let strArr = data.split(`\r\n`);

    strArr.forEach((str, i) => {
        let strCard = str.split(', ');

        if (strCard[0].includes('GeForce')) {
            let res = {
                name: strCard[0],
                uuid: strCard[1],
                server_id: serverId,
                Fan: strCard[2].split(' ').join(''),
                Temp: strCard[3] + 'C',
                Perf: strCard[4],
                Pwr: strCard[5].split(' ').join('') + ' / ' + strCard[6].split(' ').join(''),
                MemUsage: strCard[7].split(' ').join('') + ' / ' + strCard[8].split(' ').join(''),
                GpuUtil: strCard[9].split(' ').join('')
            };
            // console.log(1);
            cardsData.push(res);

            if (init) {
                initTableCards(res.name, res.uuid, res.server_id);
            }
        }
    });
}

function initTableCards(name, uuid, server_id) {
    let query = `insert into cards (cards.name, cards.serial_num, cards.server_id) values ` +
        `('${name}', '${uuid}', '${server_id}');`;

    // console.log(query);
    connection.query(query, (err, results, fields) => {
        if (err) {
            // console.log(err);
        } else {
            // console.log(results);
        }
    });
}


module.exports = function (app) {

    app.get('/', (req, res) => {
        res.send('test');;
    });

    //получить список серверов
    app.get('/servers', (req, res) => {
        connection.query("select id, name from servers;", (err, results, fields) => {
            if (err) {
                res.send({ 'error': 'oops, problems? check this: ' + err });
            } else {
                res.send(results);
            }

        });
    });

    //получить список карт 
    app.get('/cards', (req, res) => {
        connection.query("select * from cards;", (err, results, fields) => {
            if (err) {
                res.send({ 'error': 'oops, problems? check this: ' + err });
            } else {
                // console.log(results);

                results.forEach(card => {
                    cardsData.forEach(data => {
                        if (data.uuid == card.serial_num) {
                            card.params = {
                                Fan: data.Fan,
                                Temp: data.Temp,
                                Perf: data.Perf,
                                Pwr: data.Pwr,
                                MemUsage: data.MemUsage,
                                GpuUtil: data.GpuUtil
                            }
                        }
                    });
                });
                res.send(results);
            }
        });
    });

    //получить список пользователей 
    app.get('/users', (req, res) => {
        connection.query("select * from users;", (err, results, fields) => {
            if (err) {
                res.send({ 'error': 'oops, problems? check this: ' + err });
            } else {
                res.send(results);
            }

        });
    });

    //получить список процессов 
    app.get('/processes', (req, res) => {
        connection.query("select processes.id, processes.desc, processes.time_end, processes.cards, users.login from processes join users on users.id = processes.user_id;", (err, results, fields) => {
            if (err) {
                res.send({ 'error': 'oops, problems? check this: ' + err });
            } else {
                let processes = results;
                connection.query("select cards.id, cards.name, servers.host from cards join servers on cards.server_id = servers.id;",
                    (err, results, fields) => {
                        if (err) {
                            // res.send({ 'error': 'oops, problems? check this: ' + err });
                        } else {
                            res.send([processes, results]);
                        }
                    });

                ;
            }

        });
    });

    //получить процесс по id 
    app.post('/process-by-id', (req, res) => {
        connection.query(`select processes.desc, processes.time_end, processes.cards, users.id as user_id from processes join users on users.id = processes.user_id where processes.id = ${req.body.id};`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': 'oops, problems? check this: ' + err });
            } else {
                res.send(results);
            }

        });
    });

    // освободить карты при обновлении процесса 
    app.post('/free-cards', (req, res) => {
        console.log('free cards', req.body.cardsId);
        req.body.cardsId.forEach(id => {
            connection.query(`update cards set cards.status = 'free' where id = ${id};`, (err, results, fields) => {
                if (err) {
                    console.log(123, err);
                    // res.send({ 'error': 'oops, problems? check this: ' + err });
                } else {
                    console.log('НЕТ ОШИБКИ ОСВОБОЖДЕНИЯ КАРТ' + results);

                }

            });
        });
        res.send();
    });

    //обновить процесс 
    app.post('/update-task', (req, res) => {
        console.log('upd');
        connection.query(`update processes ` +
            `set processes.desc = '${req.body.desc}', processes.time_end = ${req.body.time_end}, ` +
            `processes.cards = '${req.body.cards}', processes.user_id = ${req.body.user} ` +
            `where id = ${req.body.id};`, (err, results, fields) => {
                if (err) {
                    console.log(err);
                    res.send({ 'error': 'oops, problems? check this: ' + err });
                } else {
                    res.send(results);
                }

            });

        console.log(req.body.cardsId);
        req.body.cardsId.forEach(id => {
            connection.query(`update cards set status = 'occupied' where id = ${id};`, (err, results, fields) => {
                if (err) {
                    // console.log('ОШИБКА ДОБАВЛЕНИЯ - ' + err);
                    // res.send({ 'error': 'oops, problems? check this: ' + err });
                } else {
                    // console.log('НЕТ ОШИБКИ ДОБАВЛЕНИЯ' + results);
                    // res.send(results);
                }

            });
        });
    });

    // добавить процесс
    app.post('/add-task', (req, res) => {
        console.log('add');

        let query = `insert into processes (processes.desc, processes.time_end, processes.cards, processes.user_id) values ` +
            `('${req.body.desc}', ${req.body.time_end}, '${req.body.cards}', ${req.body.user});`;
        console.log(query);

        connection.query(query, (err, results, fields) => {
            if (err) {
                console.log('НЕУДАЧА ДОБ.', err);
                // res.send({ 'error': 'oops, problems? check this: ' + err });
            } else {
                // console.log('УСПЕХ ДОБ.');
                res.send(results);
            }

        });

        console.log(req.body.cardsId);
        req.body.cardsId.forEach(id => {
            connection.query(`update cards set status = 'occupied' where id = ${id};`, (err, results, fields) => {
                if (err) {
                    // console.log('ОШИБКА ДОБАВЛЕНИЯ - ' + err);
                    // res.send({ 'error': 'oops, problems? check this: ' + err });
                } else {
                    // console.log('НЕТ ОШИБКИ ДОБАВЛЕНИЯ' + results);
                    // res.send(results);
                }

            });
        });


    });

    // удалить задачу
    app.post('/delete-task', (req, res) => {
        console.log(`delete from processes where id = ${req.body.processId};`);
        connection.query(`delete from processes where id = ${req.body.processId};`, (err, results, fields) => {
            if (err) {
                // console.log('НЕУДАЧА УД.', err);
                // res.send({ 'error': 'oops, problems? check this: ' + err });
            } else {
                // console.log('УСПЕХ УД.');
                // console.log(results);
                res.send(results);
            }

        });

        // console.log(req.body.cardsId.split(','));

        req.body.cardsId.split(',').forEach(id => {
            connection.query(`update cards set status = 'free' where id = ${id};`, (err, results, fields) => {
                if (err) {
                    // console.log('ОШИБКА УДАЛЕНИЯ - ' + err);
                    // res.send({ 'error': 'oops, problems? check this: ' + err });
                } else {
                    // console.log('НЕТ ОШИБКИ УДАЛЕНИЯ' + results);
                    // res.send(results);
                }

            });
        });
    });
}