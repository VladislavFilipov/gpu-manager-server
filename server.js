const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

const app = express();
const port = 3001;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ origin: '*' }));

const route = require('./routes');
route(app);



app.listen(port, () => {
    console.log('\n\n\n\n\nЗапущено. Порт: ' + port);
});
