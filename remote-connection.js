/*jshint node: true, esversion: 6*/
'use strict';
const pty = require('node-pty');

class RemoteConnection {

  /**
   * RemoteConnection constructor
   * 
   * @constructor
   * @this {RemoteConnection}
   * 
   * @param {string} type - type of connection {ssh1/ssh2/telnet}
   * @param {object} params - connection parameters [host, port, username, password]
   * @param {array} callback - function with parameter - `conn` `<RemoteConnection>`
   * 
   * @return {RemoteConnection}
   */
  constructor(type, params, callback) {
    //validate params
    let verdict = this.validateParams(params);
    if (!verdict.result) {
      callback({ code: 3, error: new Error(verdict.errors.join('\n')) });
    }

    let spawnProcess = '';
    let processArgs = [];
    let isWaitPassword = params.password.length;
    switch (type) {
      case 'ssh1': {
        //Example: ssh -1 -c des student@172.16.118.166 -p 22
        spawnProcess = `ssh`;
        processArgs.push(`-1`);
        processArgs.push(`-c`);
        processArgs.push(`des`);
        processArgs.push(`${params.username}@${params.host}`);
        processArgs.push(`-p`);
        processArgs.push(`${params.port}`);
        processArgs.push(`-o`);
        processArgs.push(`UserKnownHostsFile=/dev/null`);
        processArgs.push(`-o`);
        processArgs.push(`StrictHostKeyChecking=no`);

        break;
      }
      case 'ssh2': {
        //Example: ssh administrator@172.16.117.209 -p 22
        spawnProcess = `ssh`;
        processArgs.push(`${params.username}@${params.host}`);
        processArgs.push(`-p`);
        processArgs.push(`${params.port}`);
        processArgs.push(`-o`);
        processArgs.push(`UserKnownHostsFile=/dev/null`);
        processArgs.push(`-o`);
        processArgs.push(`StrictHostKeyChecking=no`);
        break;
      }
      case 'telnet': {
        //Example: telnet 172.16.118.190 23
        spawnProcess = `telnet`;
        processArgs.push(`${params.host}`);
        processArgs.push(`${params.port}`);
        break;
      }
      default: {
        callback({ code: 1, error: new Error('Remote connection type incorrect!') });
      }
    }

    this.terminal = pty.spawn(spawnProcess, processArgs, {
      name: 'xterm-color',
      cols: 80,
      rows: 30,
      cwd: process.env.HOME,
      env: process.env
    });

    let dataEventHandler = (data) => {
      //if ask password, write password
      let isWritePassword = new RegExp('assword:');
      let isConfirmKey = new RegExp('Are you sure you want to continue connecting (yes/no)?');
      let isWrongPassword = new RegExp('Permission denied, please try again');

      if (isConfirmKey.test(data)) {
        this.terminal.write(`yes\n`);
      }
      else if (isWrongPassword.test(data)) {
        callback({ code: 4, error: new Error(`Error: Wrong password!`) });
        isWaitPassword = true;
        this.close();
      }
      else if (isWritePassword.test(data)) {
        this.terminal.write(`${params.password}\n`);
        isWaitPassword = false;
      }
      else if (!isWaitPassword && data.length > 2) {
        //else return terminal with established remote connection
        this.terminal.removeListener('data', dataEventHandler);
        this.terminal.removeListener('error', errorEventHandler);
        callback(null, this);
      }
    };

    let errorEventHandler = (err) => {
      callback({ code: 2, error: new Error(`Error: ${err}`) });
    };

    this.terminal.on('data', dataEventHandler);
    this.terminal.on('error', errorEventHandler);
  }

  /**
   * Create event handler
   * 
   * @method
   * @this {RemoteConnection}
   * 
   * @param {string} event - name of event
   * @param {function} callback function event handler 
   */
  on(event, handler) {
    this.terminal.on(event, handler);
  }

  /**
   * Validate parameters for established connection
   * @method
   * @this {RemoteConnection}
   * 
   * @param {object} params - connection parameters [host, port, username, password]
   * 
   * @return {object} verdict - result of check
   */
  validateParams(params) {
    let verdict = {
      result: true,
      errors: []
    };

    if (!/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(params.host)) {
      verdict.result = false;
      verdict.errors.push("Parameter host is not valid!");
    }

    if (!(params.port >= 0 && params.port <= 65535)) {
      verdict.result = false;
      verdict.errors.push("Parameter port is not valid!");
    }

    if (!/^[A-z][A-z0-9_-]{1,40}$/.test(params.username)) {
      verdict.result = false;
      verdict.errors.push("Parameter username is not valid!");
    }

    if (!/^[A-z0-9_\!\?\@-]{0,40}$/.test(params.password)) {
      verdict.result = false;
      verdict.errors.push("Parameter password is not valid!");
    }

    return verdict;
  }

  /**
   * Write command to pty
   * 
   * @method
   * @this {RemoteConnection}
   * 
   * @param {string} - full or part of command
   */
  write(command) {
    this.terminal.write(command);
  }

  /**
   * Terminal resize
   * 
   * @method
   * @this {RemoteConnection}
   * 
   * @param {number} cols - new count of columns
   * @param {number} rows - new count of rows
   */
  resize(cols, rows) {
    this.terminal.resize(cols, rows);
  }

  /**
   * Terminal close
   * 
   * @method
   * @this {RemoteConnection}
   */
  close() {
    console.log('Closed terminal ' + this.terminal.pid);
    this.terminal.kill();
  }
}

module.exports = RemoteConnection;

//Example
// let params = {
//   host: '172.16.117.209',
//   port: 24,
//   username: 'administrator',
//   password: 'Spb78sts!'
// };

// let conn = new RemoteConnection('ssh2', params, (err, conn) => {
//   if (err) throw err;

//   conn.on('data', (data) => {
//     console.log(`${ data }`);
//   });

//   conn.on('error', (err) => {
//     throw new Error(`Error: ${err}`);
//   });

//   conn.on('close', (data) => {
//     console.log(`Close: ${ data }`);
//   });
// });

// setInterval(() => {
//     conn.write(`uptime\n`);
// }, 5000);